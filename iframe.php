<?php

// CONFIG
$CACHE_TXS = 3600; // txs cache duration in seconds (0 to disable)
$CACHE_UD = 86400; // UD cache duration in seconds (0 to disable)
$CACHE_DIR = 'cache/';
$ALLOW_CLIENT_NODE = false; // set true is security problem
$NODE = array('https://g1.duniter.org', 'https://g1.librelois.fr', 'https://g1.cgeek.fr', 'https://duniter.normandie-libre.fr', 'https://g1.presles.fr', 'https://g1.mithril.re');
$FONT_COLOR = '#212529';
$BG_COLOR = '#FFF';
$BORDER_COLOR = '#343a40';
$PROGRESS_COLOR = '#ffc107';

	if(($CACHE_TXS != 0 or $CACHE_UD != 0) and !file_exists($CACHE_DIR))
		mkdir($CACHE_DIR);
	
	if(isset($_GET['lang'])) {
		if(empty($_GET['lang'])) $langdetect = true;
		else $lang = $_GET['lang'];
	}
	require('lib/locales.php');
	include('lib/phpqrcode/qrlib.php');
	$api_node = 'g1.duniter.fr';
	$format_pubkey = '#^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{43,44}$#';
	$units = ['quantitative','relative'];
	date_default_timezone_set('Europe/Paris');
	$today = new DateTime();
	$format = 'Y-m-d';
	function isDate(&$date, $format){
		$champsDate = date_parse_from_format($format, $date);
		$date = DateTime::createFromFormat($format, $date);
		return checkdate($champsDate['month'], $champsDate['day'], $champsDate['year']);
	}
	
	// Vérification du node et des couleurs
	$nodes = ($ALLOW_CLIENT_NODE and isset($_GET['node']) and !empty($_GET['node']))? explode(' ', $_GET['node']) : $NODE;
	$font_color = (!empty($_GET['font_color'])) ? '#' . $_GET['font_color'] : $FONT_COLOR;
	$background_color = (!empty($_GET['background_color'])) ? '#' . $_GET['background_color'] : $BG_COLOR;
	$border_color = (!empty($_GET['border_color'])) ? '#' . $_GET['border_color'] : $BORDER_COLOR;
	$progress_color = (!empty($_GET['progress_color'])) ? '#' . $_GET['progress_color'] : $PROGRESS_COLOR;
	
	// Vérification des dates et calcul du nombre de jours entre la date du jour et la date de fin
	if (!empty($_GET['start_date'])){
		$start_date = $_GET['start_date'];
		$classcol = 'col-4';
		if (!isDate($start_date, $format)){
			echo '<div>'.tr('error_date_start_invalid').'</div>';
			exit;
		}
		if (!empty($_GET['end_date'])){
			if ($_GET['end_date'] !=0){
				$end_date = $_GET['end_date'];
				if (!isDate($end_date, $format)){
					echo '<div>'.tr('error_date_end_invalid').'</div>';
					exit;
				}elseif ($end_date < $start_date) {
					echo '<div>'.tr('error_date_end_invalid').'</div>';
					exit;
				}
			}
		}
	}else{
		// Si pas de start_date, on prend le mois courant
		$start_date = new DateTime('first day of this month');
		$end_date = new DateTime('last day of this month');
	}
	$start_date->setTime(0,0,0);
	if (isset($end_date)){
		$classcol = 'col-3';
		if ($end_date < $today){
			$days_left = 0;
		}else{
			$dteDiff  = $end_date->diff($today);
			$days_left = $dteDiff->format('%a');
		}
	}
	
	// Vérification du format de la pubkey
	if (!empty($_GET['pubkey'])){
		if (preg_match($format_pubkey, $_GET['pubkey'])){
			$pubkey = $_GET['pubkey'];
			$contribution_url = 'https://' . $api_node . '/api/#/v1/payment/' . $pubkey . '?amount=10|20|50|100|1000&amp;comment=don&amp;redirect_url=https%3A%252F%252F' . $api_node . '&amp;cancel_url=https%3A%252F%252F' . $api_node;
			$display_pubkey = (!empty($_GET['display_pubkey']));
			$display_button = (!empty($_GET['display_button']));
			$display_graph = (!empty($_GET['display_graph']));
			// Génération du QRcode
			$display_qrcode = (!empty($_GET['display_qrcode']));
			$qrcode_path = 'img/qrcodes/' . $pubkey . '.png';
			if ($display_qrcode && !file_exists($qrcode_path)) {
				QRcode::png($pubkey, $qrcode_path);
			}
		}
		else {
			echo '<div>'.tr('error_pubkey_invalid').'</div>';
			exit;
		}
	}else{
		echo '<div>'.tr('error_pubkey_missing').'</div>';
		exit;
	}

	// Vérification du format de la valeur cible à atteindre
	if (!empty($_GET['target'])){
		$target = (int)$_GET['target'];
		if (!is_int($target)){
			echo '<div>'.tr('error_target_not_int').'</div>';
			exit;
		}elseif ($target<=0){
			echo '<div>'.tr('error_target_negative').'</div>';
			exit;
		}
	}else{
		echo '<div>'.tr('error_target_missing').'</div>';
		exit;
	}
	
	// Récupération des transactions entrantes entre la date de début et la date du jour
	$start_timestamp = $start_date->getTimestamp();
	$today_timestamp = $today->getTimestamp();
	
	$json = null;
	$cache_filename = $CACHE_DIR . $pubkey . '_' . $start_timestamp . '.json';
	if($CACHE_TXS != 0 and file_exists($cache_filename) and time()-filemtime($cache_filename) < $CACHE_TXS) {
		$json = file_get_contents($cache_filename);
	}
	if(!$json) {
		$url_json = '/tx/history/' . $pubkey . '/times/' . $start_timestamp . '/' . $today_timestamp;
		shuffle($nodes); // shuffle for load balancing
		foreach($nodes as &$node) {
			// Test each node and keep the first which works
			$json = @file_get_contents($node . $url_json, false, stream_context_create(array('http'=>array('timeout'=>10.0))));
			if($json !== false)
				break;
		}
		if ($json === false){
			echo '<div>'.tr('error_connect_node').'</div>';
			exit;
		}
		if($CACHE_UD != 0) {
			file_put_contents($cache_filename, $json);
		}
	}
	
	$json = json_decode($json);
	$transactions = $json->history->received;
	$total = 0;
	$donneurs = [];
	$tmp_timestamp = $start_timestamp;
	$array_final = [];
	$map_delta = [];
	foreach ($transactions as $transaction){
		$donneur = $transaction->issuers[0];
		if ($donneur != $pubkey){
			if(!in_array($donneur, $donneurs)){
				array_push($donneurs, $donneur);
			}
			$outputs = $transaction->outputs;
			foreach ($outputs as $output){
				if (strstr($output,$pubkey)){
					$timestamp = $transaction->time;
					$output = explode(':', $output);
					$montant_transaction = $output[0]/100;
					if ($timestamp != $tmp_timestamp){
						array_push($array_final, ['t'=>$tmp_timestamp*1000, 'y'=>(string) $total]);
					}
					$tday = 43200 + $timestamp - $timestamp%86400;
					if(isset($map_delta[$tday]))
						$map_delta[$tday] += intval($montant_transaction);
					else
						$map_delta[$tday] = intval($montant_transaction);
					$total += $montant_transaction;
					$tmp_timestamp = $timestamp;
				}
			}
		}
	}
	$donors = count($donneurs);
	if ($display_graph){
		// On complète le tableau avec la dernière transaction et pour la date de visualisation du graphe
		array_push($array_final, ['t'=>$tmp_timestamp*1000, 'y'=>(string) $total]);
		array_push($array_final, ['t'=>$today_timestamp*1000, 'y'=>(string) $total]);
		$array_line = [['t'=>$start_timestamp*1000, 'y'=>(string) $target],['t'=>$today_timestamp*1000, 'y'=>(string) $target]];
		
		$array_delta = array();
		foreach($map_delta as $k=> $v) {
			$array_delta[] = array('t'=>$k*1000, 'y'=>$v);
		}
	}

	// Vérification de l'unité
	$unit = (!empty($_GET['unit'])) 
	    ? ((!in_array($_GET['unit'], $units)) ? 'quantitative' : $_GET['unit'])
		: 'quantitative';
	
	// Si l'unité est relative
	if ($unit == 'relative'){
		$ud = null;
		if($CACHE_UD != 0 and file_exists($CACHE_DIR . 'ud.txt') and time()-filemtime($CACHE_DIR . 'ud.txt') < $CACHE_UD) {
			$ud = floatval(file_get_contents($CACHE_DIR . 'ud.txt'));
		}
		
		if(!$ud) {
			// On récupère le dernier bloc qui contient le DU
			$url_json = $node . '/blockchain/with/ud';
			if(isset($node))
				$json = file_get_contents($url_json);
			else {
				shuffle($nodes); // shuffle for load balancing
				foreach($nodes as &$node) {
					// Test each node and keep the first which works
					$json = @file_get_contents($node . $url_json, false, stream_context_create(array('http'=>array('timeout'=>10.0))));
					if($json !== false)
						break;
				}
				if ($json === false){
					echo '<div>'.tr('error_connect_node').'</div>';
					exit;
				}
			}
			$json = json_decode($json);
			$last_block_with_ud = end($json->result->blocks);
			
			// Puis on récupère le montant du DU pour mettre à jour les données
			$url_json = $node . '/blockchain/block/' . $last_block_with_ud;
			$json = file_get_contents($url_json);
			$json = json_decode($json);
			$ud = $json->dividend/100;
			
			if($CACHE_UD != 0) {
				file_put_contents($CACHE_DIR . 'ud.txt', strval($ud));
			}
		}
		
		$total = round($total/$ud);
		if ($display_graph){
			for($i=0;$i<count($array_final);$i++){
				$array_final[$i]['y'] = round($array_final[$i]['y']/$ud,2);
			}
			for($i=0;$i<count($array_delta);$i++){
				$array_delta[$i]['y'] = round($array_delta[$i]['y']/$ud,2);
			}
		}
	}
	$percentage = round($total/$target*100);
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<head>
	<meta charset="utf-8">
	<title><?php if (!empty($_GET['title'])){echo $_GET['title'];}else{echo tr('title_default');} ?></title>
	<link rel="stylesheet" href="styles/iframe.css">
</head>
<body style="color:<?php echo $font_color; ?>;background-color:<?php echo $background_color; ?>;">
	<section class="wrapper-numbers">
		<div class="container">
			<div class="row">
				<div class="col">
					<?php if (!empty($_GET['title'])){echo '<h1>' . $_GET['title'] . '</h1>';} ?>
					<?php if ($display_pubkey){echo '<h2 class="pubkey">' . tr('label_pubkey') . ' ' . $pubkey . '</h2>';} ?>
					<?php if ($display_qrcode){echo '<img src="' . $qrcode_path . '" alt="'.tr('label_qrcode').'">';} ?>
					<div id="progressbar" class="progress border rounded-0" style="border-color:<?php echo $border_color; ?> !important;">
						<div class="progress-bar progress-bar-animated" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;background:<?php echo $progress_color; ?> !important;"><span class="sr-only">0%</span></div>
					</div>
				</div>
			</div>
			<div class="row countup text-center">
				<div class="column <?php echo $classcol; ?>">
					<p><i class="fas fa-chart-pie" aria-hidden="true"></i></p>
					<p> <span class="count"><?php echo $percentage?></span><span class="sup">%</span></p>
					<h3><?php echo tr('label_reached'); ?></h3>
				</div>
				<div class="column <?php echo $classcol; ?>">
					<p><i class="far fa-user" aria-hidden="true"></i></p>
					<p> <span class="count"><?php echo $donors; ?></span></p>
					<h3><?php echo tr('label_donators'); ?></h3>
				</div>
				<div class="column <?php echo $classcol; ?>">
					<p><i class="far fa-money-bill-alt" aria-hidden="true"></i></p>
					<p> <span class="count"><?php echo $total; ?></span><?php if ($unit == 'relative'){echo ' '.tr('ud');} ?><span class="sub">Ğ1</span></p>
					<h3><?php echo tr('label_ontotal', array('total'=>$target, 'unit'=>(($unit=='relative') ? tr('ud').'<span class="sub">Ğ1<br></span>' : 'Ğ1'))); ?></h3>
				</div>
				<?php if (isset($end_date)){ ?>
				<div class="column col-3">
					<p><i class="far fa-calendar-alt" aria-hidden="true"></i></p>
					<p> <span class="count"><?php echo $days_left; ?></span></p>
					<h3><?php echo tr('label_remaining_d'); ?></h3>
				</div>
				<?php } ?>
			</div>
			<?php if ($display_button){ ?>
			<div class="row">
				<div class="col">
					<a class="btn btn-success" href="<?php echo $contribution_url; ?>" target="_blank" role="button" style="width: 100%;">
						<i class="fas fa-check"></i><span>&nbsp;<?php echo tr('label_contribute'); ?></span>
					</a>
				</div>
			</div>
			<?php } ?>
		</div>
	</section>
	<?php if ($display_graph){ ?>
	<script src="lib/js/moment.min.js"></script>
	<script src="locales/moment.js/<?php echo $lang; ?>.js"></script>
	<script src="lib/js/chart.min.js"></script>
	<script>
	window.onload = function() {
		moment.locale('<?php echo $lang; ?>');
		var currentLocaleData = moment.localeData();
		var dateFormat = currentLocaleData.longDateFormat('L');
		var hourFormat = currentLocaleData.longDateFormat('LT');
		var container = document.querySelector('.container');
		var div = document.createElement('div');
		div.classList.add('chart-container');
		var canvas = document.createElement('canvas');
		div.appendChild(canvas);
		container.appendChild(div);
		var chartData = {
			datasets: [{
				type: "line",
				label: "<?php echo tr('graph_target'); ?>",
				data: <?php echo json_encode($array_line); ?>,
				borderColor: '#FF3E3D',
				borderDash: [5, 5],
				radius: 0,
				fill: false
			},{
				type: "bar",
				label: '<?php echo tr('graph_delta'); ?>',
				data: <?php echo json_encode($array_delta); ?>,
				borderColor: 'rgba(0,200,100,0.7)',
				backgroundColor: 'rgba(96,200,120,0.7)',
				borderWidth: 2
			},{
				type: "line",
				label: '<?php echo tr('graph_title'); ?>',
				data: <?php echo json_encode($array_final); ?>,
				borderColor: '#0099FF',
				backgroundColor: '#80CCFF',
				lineTension: 0,
				pointRadius: 1,
				borderWidth: 2,
				steppedLine: true
			}]
		};
		new Chart(canvas.getContext('2d'), {
			type: 'bar',
			data: chartData,
			options: {
				responsive: true,
				animation: {
					duration: 1800,
					easing: 'easeInCubic'
				},
				title: {
					display: true
				},
				scales: {
					xAxes: [{
						type: 'time',
						time: {
							minUnit: 'day',
							tooltipFormat: dateFormat + ' - ' + hourFormat
						}
					}]
				},
				tooltips: {
					intersect: false
				}
			}
		});
	}
	</script>
	<?php } ?>
	<script src="lib/js/jquery-3.4.1.min.js"></script>
	<script src="lib/js/counter.js"></script>
	<script>
	$(document).ready(function(){	
		$('.progress-bar').animate({
			width: '<?php echo $percentage; ?>%'
		}, 1300, '');
	});
	</script>
</body>
</html>
