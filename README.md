# Barre de financement intégrable

Générateur de barre de financement intégrable dans une page web pour suivre l'évolution d'un financement participatif.

![Capture](img/capture.jpg)

## Installation

* Déposez l'ensemble des fichiers dans un dossier sur votre serveur
* Paramétrez votre serveur web (Apache ou Nginx) pour rendre le dossier accessible depuis le web
* Les dossiers `img/qrcodes` et `cache` doivent être accessibles en écriture pour le compte utilisé par votre serveur web (en général `www-data`)
* Le générateur sera alors accessible via http(s)://votredomaine.com/votre_chemin/generate.php
